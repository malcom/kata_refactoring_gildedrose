# Gilded Rose Refactoring Kata
A refactoring Kata, bases on existing code.

## Get Started

### Requirements

- Knowledge of language Java.
- download 
  [Android Studio ide](https://developer.android.com/studio)

### Installations

1. clone the project
   ```bash 
   git clone https://gitlab.com/malcom/kata_refactoring_gildedrose.git
   ```

2. Compile and Run the project

    - Open Android Studio, open the project via the option __Open an existing Android Studio project__
    - Open the file __TexttestFixture.java__ and then right click on and choose __Run 'TexttestFixture.main()'__

### Usage

   ```bash
       public static void main(String[] args)
        System.out.println("OMGHAI!");

        //TODO ADD ITEM TO TEST HERE 
        Item[] items = new Item[] {
                new Item("+5 Dexterity Vest", 10, 20) //this an example 
                }

        GildedRose app = new GildedRose(items);

    ```

   
