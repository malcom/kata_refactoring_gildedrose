package com.gildedrose;

import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedRoseTest {

    @Test public void Should_UpdateItemQualityAndSellIn_When_Inventory() {
        Item[] items = new Item[] {
                new Item("decrease item quality", 0, 1),
                new Item("item quality is never negative", 0, 0),
                new Item("increase Aged Brie quality", 2, 15),
                new Item("quality max is 50, Aged Brie", 0, 50),
                new Item("item Sulfuras", 2, 20),
                new Item("increase item Backstage passes by 2", 10, 10),
                new Item("increase item Backstage passes by 3", 5, 10),
                new Item("increase item Backstage passes like Aged Brie", 11, 11),
                new Item("decrease Conjured item ", 1, 4),
                new Item("increase item Backstage passes by 2", 9, 49),
                new Item("decrease sellIn of other item", 7, 4),
                new Item("decrease Conjured item ", 1, 1)
        };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals(0, items[0].quality);
        assertEquals(0, items[1].quality);
        assertEquals(16, items[2].quality);
        assertEquals(50, items[3].quality);
        assertEquals(80, items[4].quality);
        assertEquals(12, items[5].quality);
        assertEquals(13, items[6].quality);
        assertEquals(12, items[7].quality);
        assertEquals(2, items[8].quality);
        assertEquals(50, items[9].quality);
        assertEquals(0, items[11].quality);
        assertEquals(8, items[9].sellIn);
        assertEquals(0, items[8].sellIn);
        assertEquals(2, items[4].sellIn);
        assertEquals(1, items[2].sellIn);
        assertEquals(6, items[10].sellIn);
    }

    @Test public void Should_IndicateCorrectCase_When_BackstagePasses() {
        Item[] items = new Item[] {
                new Item("increase by 1", 11, 11),
                new Item("increase  by 3", 4, 10),
                new Item("increase  by 2", 9, 10)
        };
        GildedRose app = new GildedRose(items);
        assertEquals(1, app.howIncreaseBackstagePasses(items[0].sellIn));
        assertEquals(3, app.howIncreaseBackstagePasses(items[1].sellIn));
        assertEquals(2, app.howIncreaseBackstagePasses(items[2].sellIn));
    }

    @Test public void Should_IndicateCorrectItemType_When_GetItem() {
        Item[] items = new Item[] {
                new Item(" item Sulfuras", 0, 15),
                new Item(" item Conjured", 0, 15),
                new Item(" item Backstage passes", 0, 15),
                new Item(" item Aged Brie", 0, 15),
                new Item("other", 0, 1)
        };
        GildedRose app = new GildedRose(items);
        assertEquals(1, app.getItemType(items[0].name));
        assertEquals(2, app.getItemType(items[1].name));
        assertEquals(3, app.getItemType(items[2].name));
        assertEquals(4, app.getItemType(items[3].name));
        assertEquals(5, app.getItemType(items[4].name));
    }
}
