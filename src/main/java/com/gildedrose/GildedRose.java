package com.gildedrose;

import static com.gildedrose.GildedRoseConstants.*;

class GildedRose {
    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (int i = 0; i < items.length; i++) {
            switch (getItemType(items[i].name)) {
                case 1: items[i].quality = ITEM_SULFURAS_QUALITY;
                    break;
                case 2:
                    items[i].quality = items[i].quality - 2;
                    if(items[i].quality < QUANTITY_MIN) items[i].quality = QUANTITY_MIN;
                    items[i].sellIn--;
                    break;
                case 3:
                    switch (howIncreaseBackstagePasses(items[i].sellIn)) {
                        case 1:items[i].quality++;
                            break;
                        case 2:items[i].quality += INCREASE_BY_2;
                            break;
                        case 3:items[i].quality += INCREASE_BY_3;
                            break;
                    }
                    if(items[i].quality > QUANTITY_MAX)
                        items[i].quality = QUANTITY_MAX;
                    items[i].sellIn--;
                    break;
                case 4:
                    if(items[i].quality < QUANTITY_MAX)
                        items[i].quality++;
                    items[i].sellIn--;
                    break;
                case 5:
                    if(items[i].quality > QUANTITY_MIN)
                        items[i].quality--;
                    items[i].sellIn--;
                    break;
            }
        }
    }

    public Integer getItemType(String name) {
        if (name.contains(ITEM_SULFURAS)) return ITEM_TYPE_SULFURAS;
        if (name.contains(ITEM_CONJURED)) return ITEM_TYPE_CONJURED;
        if (name.contains(ITEM_BACKSTAGE_PASSES)) return ITEM_TYPE_BACKSTAGE_PASSES;
        if (name.contains(ITEM_AGED_BRIE)) return ITEM_TYPE_AGED_BRIE;
        return ITEM_TYPE_OTHERS;
    }

    public Integer howIncreaseBackstagePasses(Integer sellIn) {
        if (sellIn <= BACKSTAGE_PASSES_AT_LEAST_5)
            return INCREASE_BY_3;
        else if (sellIn <= BACKSTAGE_PASSES_AT_LEAST_10)
            return INCREASE_BY_2;
        return INCREASE_BY_1;
    }
}