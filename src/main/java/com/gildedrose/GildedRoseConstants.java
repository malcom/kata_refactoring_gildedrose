package com.gildedrose;

public class GildedRoseConstants {
    public static final String ITEM_CONJURED = "Conjured";
    public static final String ITEM_SULFURAS = "Sulfuras";
    public static final String ITEM_BACKSTAGE_PASSES = "Backstage passes";
    public static final String ITEM_AGED_BRIE = "Aged Brie";
    public static final Integer BACKSTAGE_PASSES_AT_LEAST_10 = 10;
    public static final Integer BACKSTAGE_PASSES_AT_LEAST_5 = 5;
    public static final Integer ITEM_SULFURAS_QUALITY = 80;
    public static final Integer QUANTITY_MAX = 50;
    public static final Integer QUANTITY_MIN = 0;
    public static final Integer INCREASE_BY_1 = 1;
    public static final Integer INCREASE_BY_2 = 2;
    public static final Integer INCREASE_BY_3 = 3;
    public static final Integer ITEM_TYPE_SULFURAS = 1;
    public static final Integer ITEM_TYPE_CONJURED = 2;
    public static final Integer ITEM_TYPE_BACKSTAGE_PASSES = 3;
    public static final Integer ITEM_TYPE_AGED_BRIE = 4;
    public static final Integer ITEM_TYPE_OTHERS = 5;
}
